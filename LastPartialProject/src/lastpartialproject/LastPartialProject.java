/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lastpartialproject;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author frederic
 */
public class LastPartialProject {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Random r = new Random();
        Scanner s = new Scanner(System.in);
        System.out.println("cuantos invitados hay?");
        int length = s.nextInt();
        s.nextLine();
        Guest[] gArray = new Guest[length];

        for (int i = 0; i < length; i++) {
            gArray[i] = new Guest(r.nextInt(90), NameGenetator.generateName());
            System.out.println(gArray[i].GuestID + "-" + gArray[i].Name);
        }

        Boolean askAgain = true;
        String nombre;
        while (askAgain) {
            System.out.println("Dame el Nombre del Invitado");
            nombre = s.nextLine();
            int index = -1;
            Boolean exist = false;
            for (int i = 0; i < length; i++) {
                if (gArray[i].Name.equals(nombre)) {
                    exist = true;
                    index = i;
                    break;
                }
            }

            if (exist) {

                System.out.println("Esta persona  esta invitada: " + gArray[index].GuestID + "-" + gArray[index].Name);

            } else {

                System.out.println("Esta persona no esta invitada");
            }

            System.out.println("quiere buscar a alguien mas");

            askAgain = s.nextBoolean();
            s.nextLine();
        }

        System.out.println("Attended");
        for (int i = 0; i < length; i++) {
            if (gArray[i].Attended) {
                System.out.println(gArray[i].GuestID + " - " + gArray[i].Name);
            }

        }

        System.out.println("Not Attended");
        for (int i = 0; i < length; i++) {
            if (gArray[i].Attended == false) {
                System.out.println(gArray[i].GuestID + " - " + gArray[i].Name);
            }

        }
    }

}

    
    

